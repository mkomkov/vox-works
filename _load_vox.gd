@tool
extends EditorScript


func _run():
	var vd := VoxData.new()
	
	if vd.load_vox("res://vox_data/3x3x3.vox") != OK:
		printerr("Can't load vox data")
		return

	(get_scene().get_node("MeshInstance3D") as MeshInstance3D).mesh = vd.create_vox_mesh(0)
